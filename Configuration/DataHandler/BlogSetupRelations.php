<?php

/** @var array $data */
$data = [];

// Tag and Category relation
$data['pages']['NEW_firstBlogPostPage']['tags'] = 'NEW_blogTagTYPO3';
$data['pages']['NEW_firstBlogPostPage']['categories'] = 'NEW_blogCategoryTYPO3';

return $data;
