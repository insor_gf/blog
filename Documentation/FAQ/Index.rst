Frequently Asked Questions
==========================

.. contents::
      :local:
      :depth: 1

Where to report bugs or improvements?
-------------------------------------

Please visit our `service desk`_ to report bugs or request improvements.


Slack channel
-------------

Visit us on slack in channel `#t3g-ext-blog`_


Contributions
-------------

Any contributor is welcome to join our team. All you need is an account on our bitbucket system.
Please request a user account in our `service desk`_. If you already have an account, please ask for access to project in our `service desk`_.


Clone / git repo
----------------

The git repository is public and you can clone it like every git repository:

.. code-block:: bash

   git clone https://bitbucket.typo3.com/projects/EXT/repos/blog/browse


.. _service desk: https://jira.typo3.com/servicedesk/customer/portal/3/group/21
.. _#t3g-ext-blog: https://typo3.slack.com/archives/t3g-ext-blog